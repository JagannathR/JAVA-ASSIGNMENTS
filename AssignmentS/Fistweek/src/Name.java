
import javax.swing.*;
import java.awt.*;


public class Name extends JFrame{
	private static final long serialVersionUID = 1L;
	public Name()
	{
		setSize(700,700);
        setVisible(true);
	}
	
	public static void main(String args[])
	{
		new Name();
	}
	public void paint(Graphics g)
	{
		
        g.drawLine(100, 200, 100, 50); 
        g.drawLine(20,50,180,50);
        g.drawLine(100,200,50,200);
        g.drawLine(50, 200, 50, 170);
       
        g.setColor(Color.orange);
        g.drawRect(200, 50, 80, 100);
        g.drawLine(200, 150, 200, 200);
        g.drawLine(280, 150, 280, 200);
        
        g.setColor(Color.red);
        g.drawLine(400, 50, 300, 50);
        g.drawLine(300, 50, 300, 200);
        g.drawLine(300, 200, 360, 200);
        g.drawLine(360, 200, 360, 170);
        g.drawLine(360, 170, 400, 170);
        g.drawLine(400, 170, 400, 200);
        
        g.setColor(Color.blue);
        g.drawLine(550, 50, 450, 50);
        g.drawLine(450, 50, 450, 130);
        g.drawLine(450, 130, 550, 130);
        g.drawLine(550, 130, 550, 200);
        g.drawLine(550, 200, 450, 200);
          
	}
}
