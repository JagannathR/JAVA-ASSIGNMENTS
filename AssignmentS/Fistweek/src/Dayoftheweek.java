//	A function to determine the day of the week a person was born given his or her birth date. (Hint : Consider the leap year)



import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

class Dyofweek
{
	public String s; 
	public int dow;
	public Date d;
	void input()
	{
		try
		{
			Scanner scan=new Scanner(System.in);
			System.out.println("Enter the dob in the format dd/MM/yyyy");
			s=scan.next();
		}
		catch(Exception e)
		{
			System.out.println("Error in input method");
		}
	}
	int compute()
	{
		try
		{	
			Calendar c=Calendar.getInstance();
			SimpleDateFormat f=new SimpleDateFormat("dd/MM/yyyy");
			d=f.parse(s);
			
			
			System.out.println("The date entered is:"+"\t"+s);
			System.out.println("The date without format is:"+"\t"+d);
			System.out.println("The date with format is:"+"\t"+f.format(d));
			
			c.setTime(d);
			
			dow=c.get(Calendar.DAY_OF_WEEK);
		
			SimpleDateFormat f1=new SimpleDateFormat("EEE");
			System.out.println("The day is:"+"\t"+f1.format(d));
		}
		catch(Exception e)
		{
			System.out.println("Error from method");
		}
		return dow;
	}
}

public class Dayoftheweek {
	public static void main(String args[])
	{
		try
		{
			Dyofweek d1=new Dyofweek();
			d1.input();
			int i=d1.compute();
			System.out.printf("The day of the week is:"+"\t"+i+"\n");
		}
		catch(Exception e)
		{
			System.out.println("Error from main");
		}
	}
}
