

import java.util.Scanner;


class Area
{
	Scanner scan=new Scanner(System.in);
	void square()
	{
		int side;
		double area;
		System.out.println("Enter the Length of the square");
		side=scan.nextInt();
		area=side*side;
		System.out.println("The area of the square is:"+"\t"+area+"\n");
	}
	
	void rectangle()
	{
		int length,breadth;
		double area;
		System.out.println("Enter the length of the Rectangle");
		length=scan.nextInt();
		System.out.println("Enter the breadth of the Rectangle");
		breadth=scan.nextInt();
		area=length*breadth;
		System.out.println("The area of the Rectangle is:"+"\t"+area+"\n");
	}
	
	void triangle()
	{
		int length,width;
		double area;
		System.out.println("Enter the length of the Triangle");
		length=scan.nextInt();
		System.out.println("Enter the width of the Triangle");
		width=scan.nextInt();
		area=(length*width)/2;
		System.out.println("The area of the Triangle is:"+"\t"+area+"\n");
	}
	
	void circle()
	{
		int radius;
		double area;
		System.out.println("Enter the radius of the Cirle");
		radius=scan.nextInt();
		area=3.142*radius*radius;
		System.out.println("The area of the Circle is:"+"\t"+area);
	}
}
public class Areas {
	public static void main(String args[])
	{
		Area a=new Area();
		a.square();
		
		a.rectangle();
		
		a.triangle();
		
		a.circle();
	}
}
