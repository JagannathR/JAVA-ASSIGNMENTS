											/*A Simple Graphics Program*/

import javax.swing.*;
import java.awt.*;

public class Simplegraphics extends JFrame{
	private static final long serialVersionUID = 1L;

	public Simplegraphics(){
          setSize(500,500);
          setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          setVisible(true);
     }

     public static void main(String a[]){
         new Simplegraphics();
     }

     public void paint(Graphics g){
          g.drawOval(40, 40, 60, 60); /*FOR CIRCLE*/
          
          g.setColor(Color.blue);
          g.drawRect(100, 100, 150, 150); /* FOR SQUARE*/
          
          g.setColor(Color.red);
          g.drawRect(150, 270, 250, 150); /* FOR RECTANGLE*/
          
     }
}




