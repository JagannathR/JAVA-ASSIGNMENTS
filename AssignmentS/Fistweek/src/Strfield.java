											/* Assignment 2 problem 2*/

import java.util.*;

class Keyval
{
	Scanner scan=new Scanner(System.in);
	HashMap hm=new HashMap();
	Integer key;
	public String value,keyy;
	public int n;
	
	
	public void input()
	{
		System.out.println("Enter how many key value pairs");
		n=scan.nextInt();
		
		for(int i=1;i<=n;i++)
		{
			System.out.println("Enter the unique Key");
			key=scan.nextInt();
		
			System.out.println("Enter the Value");
			value=scan.next();
			
			hm.put(key,value);
		}
		System.out.println("The values are:");
		
			System.out.println(hm);
	}
	
	
	public void compute()
	{
		System.out.println("Enter the Key to know values");
		keyy=scan.next();
		if(hm.containsKey(keyy))
		{
			System.out.println(hm.get(keyy));
		}
		else
		{
			System.out.println("No value found");
		}
	}
}


public class Strfield {
	public static void main(String args[])
	{
		Keyval k=new Keyval();
		k.input();
		
		k.compute();
	}
}
