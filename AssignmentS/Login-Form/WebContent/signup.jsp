<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="#f0f0f0">
		
		<form  action="Signup?action=Signup" method="POST">	
		<table  bgcolor="#f0f0f0">
				<tr align="center">
					<td style="color:green"><h3>New <b>USER</b> <u><b>SIGNUP</b></u></h3><br/></td>
				</tr>				
				<tr >
					<td>Employee Name:<input type="text" name="empname" size="30"></td>
				</tr>
				<tr>
				    <td>DOJ:<td><input type="text" name="doj" id="edoj" size="30"></td>
				</tr>
				<tr align="justify">
					<td>Total Experience:<input type="text" name="empexp" id="ee" size="30"></td>
				</tr>
				<tr align="justify">
					<td>PAN Card Number:<input type="text" name="emppan" id="epn" size="30"></td>
				</tr>
				<tr align="justify">
					<td>Phone Number:<input type="text" name="empno" id="eno" size="30"></td>
				</tr>
				<tr align="justify">
					<td>Current Address:<br/><textarea name="empca" id="eca" rows="5" cols="50"></textarea></td>
				</tr>
				<tr align="justify">
					<td>Present Address:<br/><textarea name="emppa" id="epa" rows="5" cols="50"></textarea></td>
				</tr>
				<tr align="justify">
					<td>Blood Group:<input type="text" name="empbg" id="ebg" size="30"></td>
				</tr>
				
			</table>
			
			<fieldset>
				<p>Emergency Contact Details</p>
				<table>
					<tr align="justify">
						<td>Emergency Contact Person:<input type="text" name="emperson" id="emp" size="30"></td>
					</tr>
					<tr align="justify">
						<td>Emergency Contact Person Number:<input type="text" name="empersonno" id="empno" size="30"></td>
					</tr>
					<tr align="justify">
						<td>Emergency Contact Person Address:<br/><textarea rows="6" cols="50"></textarea></td>
					</tr>
					<tr align="justify">
						<td>Relation With Emergency Contact Person:<input type="text" name="emprel" id="empr" size="30"></td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset>
				<p>Bank Account Details:</p>
				<table>
					<tr align="justify">
						<td>Account No:<input type="text" name="accno" id="an" size="30"></td>
					</tr>
					<tr align="justify">
						<td>IFSC Code:<input type="text" name="ifsccode" id="icode" size="30"></td>
					</tr>
					<tr align="left">
						<td>Branch:<input type="text" name="branch" id="bn" size="30"></td>
					</tr>
				</table>
				<input type="submit" name="submit">
			</fieldset>			
		</form>
	
	
</body>
</html>