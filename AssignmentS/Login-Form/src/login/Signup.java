package login;

import java.io.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String name2=request.getParameter("signup");
		
		/*Sign up details*/
		String en=request.getParameter("empname");
		String d=request.getParameter("doj");
		String te=request.getParameter("empexp");
		String pcn=request.getParameter("emppan");
		String pn=request.getParameter("empno");
		String ca=request.getParameter("empca");
		String pa=request.getParameter("emppa");
		String bg=request.getParameter("empbg");
		String ecp=request.getParameter("emperson");
		String ecpn=request.getParameter("empersonno");
		String ecpa=request.getParameter("ecpa");
		String ecpr=request.getParameter("emprel");
		String an=request.getParameter("accno");
		String ic=request.getParameter("ifsccode");
		String branch=request.getParameter("branch");
		
		if(name2.equalsIgnoreCase("signup"))
		{
				
			RequestDispatcher rd=request.getRequestDispatcher("Signup.jsp");
			rd.forward(request,response);
			
				String url="jdbc:oracle:thin:@localhost:1521:XE";
		        String usnm="system";
		        String usp="oracle";
		        Connection con=null;
		        PreparedStatement pstmt=null;
		        ResultSet res=null;
		       
		        try
		        {
		        	Class.forName("oracle.jdbc.driver.OracleDriver");
		        	
		        	con=DriverManager.getConnection(url, usnm ,usp);
		        	
		        	String s="insert into form values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		        	pstmt=con.prepareStatement(s);
		        	
		        	pstmt.setString(1,en);
		        	pstmt.setString(2,d);
		        	pstmt.setString(3,te);
		        	pstmt.setString(4,pcn);
		        	pstmt.setString(5,pn);
		        	pstmt.setString(6,ca);
		        	pstmt.setString(7,pa);
		        	pstmt.setString(8,bg);
		        	pstmt.setString(9,ecp);
		        	pstmt.setString(10,ecpn);
		        	pstmt.setString(11,ecpa);
		        	pstmt.setString(12,ecpr);
		        	pstmt.setString(13,an);
		        	pstmt.setString(14,ic);
		        	pstmt.setString(15,branch);
		        	res=pstmt.executeQuery();
		        	
		        	
		        	while(res.next()==true)
		      		{
		        		out.println("The values entered are:");
		        	//	System.out.println(en+"\n"+d+"\n"+te+"\n"+pcn+"\n"+pn+"\n"+ca+"\n"+pa+"\n"+bg+"\n"+ecp+"\n"+ecpn+"\n"+ecpa+"\n"+ecpr+"\n"+an+"\n"+ic+"\n"+branch);
		      		}
		        	
		        	RequestDispatcher red=request.getRequestDispatcher("ThankYou.jsp");
					red.forward(request, response);
					
		        }
		        catch(Exception e)
		        {
		        	out.close();
		        	System.out.println("error in");
		        }
				
		}
	}

}
