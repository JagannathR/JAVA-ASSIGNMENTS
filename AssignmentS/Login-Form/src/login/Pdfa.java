package login;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class Pdfa extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String act=request.getParameter("action");
		if("mypdf".equals(act))
		{
			response.setContentType("text/html");
			String password=request.getParameter("adpd");
			
			
		
			try
			{
				Class.forName ("oracle.jdbc.driver.OracleDriver");
				Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "oracle");
				Statement st=con.createStatement();
				ResultSet res=st.executeQuery("select count(*) from form");
            
				if(res.next()==true)
				{
					try
					{
						/* Create Connection objects */
						Class.forName ("oracle.jdbc.driver.OracleDriver");
						Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "oracle");
						Statement stmt = conn.createStatement();
						/* Define the SQL query */
						ResultSet query_set = stmt.executeQuery("SELECT * FROM form");
						/* Step-2: Initialize PDF documents - logical objects */
						Document my_pdf_report = new Document();
						PdfWriter.getInstance(my_pdf_report, new FileOutputStream("pdf_report_devsys.pdf"));
						my_pdf_report.open();            
						//	we have four columns in our table
						PdfPTable my_report_table = new PdfPTable(15);
						//	create a cell object
						PdfPCell table_cell;
						
						while (query_set.next())
						{                
							String EMPLOYEE_NAME= query_set.getString("EMPLOYEE_NAME");
							table_cell=new PdfPCell(new Phrase(EMPLOYEE_NAME));
							my_report_table.addCell(table_cell);
							
							String DOJ=query_set.getString("DOJ");
							table_cell=new PdfPCell(new Phrase(DOJ));
							my_report_table.addCell(table_cell);
                            
							String TOTAL_EXP=query_set.getString("TOTAL_EXP");
							table_cell=new PdfPCell(new Phrase(TOTAL_EXP));
							my_report_table.addCell(table_cell);
							
							String PAN_CARDNO=query_set.getString("PAN_CARDNO");
							table_cell=new PdfPCell(new Phrase(PAN_CARDNO));
                        	my_report_table.addCell(table_cell);
                            
                        	String PHONE_NO=query_set.getString("PHONE_NO");
                        	table_cell=new PdfPCell(new Phrase(PHONE_NO));
                        	my_report_table.addCell(table_cell);
                        
                        	String CURRENT_ADD=query_set.getString("CURRENT_ADD");
                        	table_cell=new PdfPCell(new Phrase(CURRENT_ADD));
                        	my_report_table.addCell(table_cell);
                        
                        	String PRESENT_ADD=query_set.getString("PRESENT_ADD");
                        	table_cell=new PdfPCell(new Phrase(PRESENT_ADD));
                        	my_report_table.addCell(table_cell);
                        
                        	String BLOOD_GRP=query_set.getString("BLOOD_GRP");
                        	table_cell=new PdfPCell(new Phrase(BLOOD_GRP));
                        	my_report_table.addCell(table_cell);
                        
                        	String EC_PERSON=query_set.getString("EC_PERSON");
                        	table_cell=new PdfPCell(new Phrase(EC_PERSON));
                        	my_report_table.addCell(table_cell);
                        
                        	String EC_PERSON_NO=query_set.getString("EC_PERSON_NO");
                        	table_cell=new PdfPCell(new Phrase(EC_PERSON_NO));
                        	my_report_table.addCell(table_cell);
                        
                        	String EC_PERSON_ADD=query_set.getString("EC_PERSON_ADD");
                        	table_cell=new PdfPCell(new Phrase(EC_PERSON_ADD));
                        	my_report_table.addCell(table_cell);
                        
                        	String ECP_REL=query_set.getString("ECP_REL");
                        	table_cell=new PdfPCell(new Phrase(ECP_REL));
                        	my_report_table.addCell(table_cell);
                        
                        	String ACC_NO=query_set.getString("ACC_NO");
                        	table_cell=new PdfPCell(new Phrase(ACC_NO));
                        	my_report_table.addCell(table_cell);
                        
                        	String IFSC_CODE=query_set.getString("IFSC_CODE");
                        	table_cell=new PdfPCell(new Phrase(IFSC_CODE));
                        	my_report_table.addCell(table_cell);
                        
                        	String BRANCH=query_set.getString("BRANCH");
                        	table_cell=new PdfPCell(new Phrase(BRANCH));
                        	my_report_table.addCell(table_cell);
                        
						}
						/* Attach report table to PDF */
						my_pdf_report.add(my_report_table);                       
						my_pdf_report.close();
            
						/* Close all DB related objects */
						query_set.close();
						stmt.close(); 
						conn.close();             
            
					}
            
					catch(Exception e)
					{
						System.out.println("1");
						e.printStackTrace();
					}
				}
				if("jaga".equals(password))
				{
					response.sendRedirect("pdf_report_Dev.pdf");
				}
			}
			
			catch(Exception e)
			{
				System.out.println("2");
				e.printStackTrace();
			}
		}
	}

}
